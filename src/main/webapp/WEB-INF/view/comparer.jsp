<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Comparer</title>

<link rel="shortcut icon" href="#">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>

<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>

<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<style type="text/css">

.carousel-item img {
	height: 250px;
}

.carousel-control-prev-icon, .carousel-control-next-icon {
	height: 100px;
	width: 100px;
	outline: black;
	background-size: 100%, 100%;
	border-radius: 50%;
	background-image: none;
}

.carousel-control-next-icon:after {
	content: '>';
	font-size: 55px;
	color: black;
}

.carousel-control-prev-icon:after {
	content: '<';
	font-size: 55px;
	color: black;
}

#text {
	color: yellow;
	font-family: monospace;
	font-size: 27px;
	font-weight: bold;
}

.center {
	text-align: center;
}

td, th {
	border: 1px solid #cccccc;
	padding: 8px;
}

th {
	font-weight: bold;
	text-transform: uppercase;
}

table {
	border-collapse: collapse;
	width: 100%;
}

tr:nth-child(even) {
	background-color: #dddddd;
}

tr:hover {
	background-color: black;
	color: white;
}

#tooltip{
position: absolute;
left:145px;
top: -223px;
background: inherit;
border-radius: 7px;
font-family: monospace;
text-align: justify;
margin: 0px;
padding: 5px;
visibility: hidden;
width:231%;
}

#tooltip:before{
content:'◄';
font-size:40px;
position: absolute;
top:212.5px;
left:-20.5px;
margin: 0px;
padding: 0px;
color: #292b2c;

}

#button-tooltip:hover #tooltip{
visibility: visible;
}



</style>

</head>
<body background="white">

	<div class="container p-1 bg-dark"></div>
	
	
	<section id="jumbotron">
		<div class="container p-2 bg-dark">
			<div class="jumbotron bg-light">
				<h1 class="display-16">Hello, savers!</h1>
				<p class="lead">Finally the application, which saves your money
					is here. Enjoy!!!</p>
				<hr class="my-16">
				<p>It is so easy, just find the best rate.</p>
				<div id="button-tooltip" class="btn btn-dark btn-lg text-white" role="button" style="position: relative">Learn more
				<p id="tooltip">Nobody has time for browsing the Internet and searching for the best rates. Just choose the currency and click on "Select".
				 Subsequently the table will pop up with all the Czech banks. 
				 Moreover, you can order the table by "Buy" or by "Sell". 
				 "Buy" means that these banks buy the selected currency. 
				 To the contrary, "Sell" means that these banks sell the selected currency.</p>
				</div>
			</div>
		</div>
		

	</section>

	<section id="findings">
		<div class="container p-2 bg-dark">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<select class="form-control form-control-lg"
							id="selectionOfCurrencies">
							<option>AUD - Australian Dollar</option>
							<option>BGN - Bulgarian Lev</option>
							<option>CAD - Canadian Dollar</option>
							<option>CHF - Swiss Franc</option>
							<option>CNY - Chinese Yuan</option>
							<option>DKK - Danish Krone</option>
							<option>EUR - EURO</option>
							<option>GBP - British Pound</option>
							<option>HRK - Croatian Kuna</option>
							<option>HUF - Hungarian Forint</option>
							<option>JPY - Japanese yen</option>
							<option>NOK - Norwegian Krone</option>
							<option>NZD - New Zealand Dollar</option>
							<option>PLN - Polish Zloty</option>
							<option>RON - Romanian New Leu</option>
							<option>RUB - Russian Ruble</option>
							<option>SEK - Swedish Krona</option>
							<option>TRY - New Turkish Lira</option>
							<option>USD - US Dollar</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<button class="btn-outline-dark btn-lg" id="button-show"
						style="width: inherit;">Select</button>
				</div>
			</div>
		</div>
	</section>

	<section id="corousel">
		<div class="container p-2 bg-dark">

			<div class="row">
				<div class="col-md-8">
					<div id="carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li id="cor.1" data-target="#carousel" data-slide-to="0" class="active"></li>
							<li id="cor.2" data-target="#carousel" data-slide-to="1" class=""></li>
							<li id="cor.3" data-target="#carousel" data-slide-to="2"></li>
							<li id="cor.4" data-target="#carousel" data-slide-to="3"></li>
							<li id="cor.5" data-target="#carousel" data-slide-to="4"></li>
							<li id="cor.6" data-target="#carousel" data-slide-to="5"></li>
							<li id="cor.7" data-target="#carousel" data-slide-to="6"></li>
						</ol>
						<div class="carousel-inner rounded">
							<div class="carousel-item active">
								<img src='<spring:url value="/images/mbank.jpg"/>'
									class="d-block w-100" alt="mBANK" title="mBank S.A">
							</div>
							<div class="carousel-item">
								<img src='<spring:url value="/images/rb.png"/>'
									class="d-block w-100" alt="RB" title="Raiffeisenbank a.s.">
							</div>
							<div class="carousel-item">
								<img src='<spring:url value="/images/kb.jpg"/>'
									class="d-block w-100" alt="KB" title="Komerční banka, a.s.">
							</div>
							<div class="carousel-item">
								<img src='<spring:url value="/images/csob.png"/>'
									class="d-block w-100" alt="CSOB"
									title="Československá obchodní banka, a. s.">
							</div>
							<div class="carousel-item">
								<img src='<spring:url value="/images/fio.jpg"/>'
									class="d-block w-100" alt="FIO" title="Fio banka, a.s.">
							</div>
							<div class="carousel-item">
								<img src='<spring:url value="/images/uni.png"/>'
									class="d-block w-100" alt="UNI"
									title="UniCredit Bank Czech Republic and Slovakia, a.s.">
							</div>
							<div class="carousel-item">
								<img src='<spring:url value="/images/equa.png"/>'
									class="d-block w-100" alt="EQUA" title="Equa bank a.s. ">
							</div>
						</div>
						<a class="carousel-control-prev" href="#carousel" role="button"
							data-slide="prev"> <span class="carousel-control-prev-icon"
							aria-hidden="true"></span> <span class="sr-only">Previous</span>
						</a> <a class="carousel-control-next" href="#carousel" role="button"
							data-slide="next"> <span class="carousel-control-next-icon"
							aria-hidden="true"></span> <span class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<div class="col-md-4" id="divtry" align="center">

					<div id="author">
						<span id="text" style="font-size: 36px;">Oto Svider</span><br />

						<a href="mailto:otosvider@seznam.cz"><img
							src='<spring:url value="/images/mail.png"/>' width="64px"
							height="64px"></a> <br /> <br /> <a
							href="https://www.linkedin.com/in/oto-svider-7121871a1"
							style="text-decoration: none;"> <img
							src='<spring:url value="/images/linkedln.png"/>' width="64px"
							height="64px">
						</a>
					</div>
					<div id="text" align="center">
						<span id="chosentext"></span>
					</div>
					<br /> <br />
					<div align="center">
						<img id="img-show" width="128px" height="128px">
					</div>
					<div align="right">
						<button class="btn-outline-dark btn-sm" id="button-hide">Hide</button>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="tableofCurrencies">
		<div class="container p-2 bg-dark">
			<div class="bg-white">
				<table id="table">
					<tr id="tableTR">
						<th>Name</th>
						<th class="center"><span id="thBuyUP"
							style="position: relative; top: -7px">▲</span><span
							id="thBuyDown"
							style="position: relative; left: -13.7px; top: 9px">▼</span>Buy</th>
						<th class="center"><span id="thSellUP"
							style="position: relative; top: -7px">▲</span><span
							id="thSellDown"
							style="position: relative; left: -13.7px; top: 9px">▼</span>Sell</th>
					</tr>

					<tbody id="tableData">

					</tbody>
				</table>
			</div>
		</div>
	</section>



</body>
<script type="text/javascript">
$(document).ready(function(){
	

	$('#table').hide();

	$('#button-hide').hide();
	
	$('#img-show').hide();

	
var banks = ${banks};

var bankMap;
	
const prefix = "/images/";
const suffix = ".png";

var filteredData =[];

function loadTableData(filteredData){

var dataHtml = '';

$('.tr-add').remove();


for (var bank of filteredData){
    dataHtml+= '<tr class="tr-add"><td>' + bank[0] + '</td><td class="center">' + bank[1] + '</td><td class="center">' + bank[2] + '</td></tr>';}

$('#tableData').append(dataHtml);
};



$('#button-show').click(function(){
	
	$.ajax({
		url : 'getData',
		success : function(data){
			banks = data;
			}
		}); 
	
bankMap = banks.map(bank => [bank.nameOfBank,bank.allRates]);

var innerText = $('#selectionOfCurrencies').val();

$('#author').hide();

const bodyImage = innerText.substring(0,3).toLowerCase();
const imagePath = prefix+bodyImage+suffix;

$('#img-show').attr("src",imagePath);

$('#img-show').show();

$('#chosentext').text(innerText);

$('#chosentext').show();


$('#button-hide').slideDown(600);

$('#divtry').show(2500);


innerText = innerText.substring(0,3);

filteredData = [];


bankMap.forEach(function(mainBank){
mainBank[1].forEach(function(bank){
if(bank.currencyName==innerText){
filteredData.push([mainBank[0],bank]);}
});
});


filteredData = filteredData.map(bank => [bank[0],bank[1].buy,bank[1].sell]);


loadTableData(filteredData);


$('#table').show();

});

	$('#thBuyUP').click(function(){
	
	filteredData = filteredData.sort((a,b) => a[1] - b[1])

	loadTableData(filteredData);
	});
	
	$('#thBuyDown').click(function(){
		
		filteredData = filteredData.sort((a,b) => b[1] - a[1])

		loadTableData(filteredData);
		});
	
	$('#thSellUP').click(function(){
		
		filteredData = filteredData.sort((a,b) => a[2] - b[2])

		loadTableData(filteredData);
		});
	
	$('#thSellDown').click(function(){
		
		filteredData = filteredData.sort((a,b) => b[2] - a[2])

		loadTableData(filteredData);
		});

	

$('#button-hide').click(function(){

$('#chosentext').hide();

$('#img-show').hide();

$('#button-hide').hide();

$('#author').show();

$('#table').hide();

});

});

</script>
</html>