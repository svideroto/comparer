<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
 <%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">

div{
color: red;
text-transform:uppercase;
font-weight: bold;
font-size: large;
width: 50%;
margin: 0px auto;
text-align: center;
}
</style>
</head>
<body>

<div>
<img src='<spring:url value="/images/outoforder.jpg"/>'><br />
We are currently repairing the error of this application.
Please be patient.
</div>

</body>
</html>