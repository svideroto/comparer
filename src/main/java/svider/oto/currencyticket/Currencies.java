package svider.oto.currencyticket;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@Scope("prototype")
public class Currencies {

	private Double buy;
	private Double sell;
	private String currencyName;
}
