package svider.oto.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import svider.oto.connector.selenium.ConnectorOfSelenium;
import svider.oto.currencyticket.Currencies;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Component
public class UNI extends ConnectorOfSelenium implements IBankComparer {

	private static final String baseURL = "https://www.unicreditbank.cz/";
	private static final String urlParams = "cs/exchange_rates_xml.exportxml.html";
	private static String URL = baseURL + urlParams;
	private WebDriver webDriver;

	@Autowired
	CurrencyTicket currencyTicket;

	Currencies currencies;

	@Lookup
	public Currencies getCurrencies() {
		return null;
	}

	@Override
	public CurrencyTicket downloadRates() throws InterruptedException {

		webDriver = this.getWebDriver(webDriver, URL);

		List<WebElement> downloadedData = Download();

		CurrencyTicket currencyTicket = Transform(downloadedData);

		closeWebDriver(webDriver);

		return currencyTicket;
	}

	public List<WebElement> Download() throws InterruptedException {

		List<WebElement> allRates = webDriver.findElements(By.tagName("exchange_rate"));

		return allRates;
	}

	public CurrencyTicket Transform(List<WebElement> downloadedData) {

		currencyTicket.setAllRates(new ArrayList<Currencies>());

		downloadedData.stream()
				.filter(webElementUNI -> webElementUNI.getAttribute("type").equals("XML_RATE_TYPE_UCB_PURCHASE_DEVIZA"))
				.collect(Collectors.toList()).get(0).findElements(By.tagName("currency")).stream()
				.forEach(webElement -> {
					currencies = this.getCurrencies();
					currencies.setCurrencyName(webElement.getAttribute("name"));
					currencies.setBuy(Double.valueOf(webElement.getAttribute("rate")));
					currencyTicket.getAllRates().add(currencies);
				});

		List<WebElement> dataForSell = downloadedData.stream()
				.filter(webElementUNI -> webElementUNI.getAttribute("type").equals("XML_RATE_TYPE_UCB_SALE_DEVIZA"))
				.collect(Collectors.toList()).get(0).findElements(By.tagName("currency"));

		int i = 0;
		for (WebElement webElementSell : dataForSell) {
			currencyTicket.getAllRates().get(i).setSell(Double.valueOf(webElementSell.getAttribute("rate")));
			i++;
		}

		String myDateStringUNI = downloadedData.get(0).getAttribute("valid_from");

		String[] myDateStringArrayUNI = myDateStringUNI.split("\\.");

		int day = 0;
		int month = 0;
		int year = 0;

		if (myDateStringUNI != null) {

			day = Integer.valueOf(myDateStringArrayUNI[0]);
			month = Integer.valueOf(myDateStringArrayUNI[1]);
			year = Integer.valueOf(myDateStringArrayUNI[2]);
		}

		LocalDate myLocalDateUNI = LocalDate.of(year, month, day);

		LocalTime myLocalTimeUNI = LocalTime.of(0, 0, 0);

		LocalDateTime myLocalDateTimeUNI = LocalDateTime.of(myLocalDateUNI, myLocalTimeUNI);

		currencyTicket.setLastRefresh(myLocalDateTimeUNI);
		currencyTicket.setBank(EnumBanks.UNI);
		currencyTicket.setNameOfBank("UniCredit Bank Czech Republic and Slovakia, a.s.");

		return currencyTicket;
	}
}
