package svider.oto.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import svider.oto.connector.selenium.ConnectorOfSelenium;
import svider.oto.currencyticket.Currencies;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Component
public class EQUA extends ConnectorOfSelenium implements IBankComparer {

	private static final String baseURL = "https://www.equabank.cz/";
	private static final String urlParams1 = "dulezite-dokumenty/";
	private static final String urlParams2 = "kurzovni-listek";

	private static String URL = baseURL + urlParams1 + urlParams2;
	private WebDriver webDriver;

	@Autowired
	CurrencyTicket currencyTicket;

	Currencies currencies;

	@Lookup
	public Currencies getCurrencies() {
		return null;
	}

	@Override
	public CurrencyTicket downloadRates() throws InterruptedException {

		webDriver = this.getWebDriver(webDriver, URL);

		WebElement downloadedData = Download();

		CurrencyTicket currencyTicket = Transform(downloadedData);

		closeWebDriver(webDriver);

		return currencyTicket;
	}

	public WebElement Download() throws InterruptedException {

		WebElement allRates = webDriver.findElement(By.id("currency"));

		return allRates;
	}

	public CurrencyTicket Transform(WebElement downloadedData) {

		currencyTicket.setAllRates(new ArrayList<Currencies>());

		downloadedData.findElement(By.tagName("tbody")).findElements(By.tagName("tr")).stream()
				.forEach(webElementAQUA -> {

					currencies = this.getCurrencies();

					String currencyName = webElementAQUA
							.findElements(By.tagName("td")).stream().filter(elementCurrencyName -> elementCurrencyName
									.getAttribute("class").equals("cell-currency"))
							.collect(Collectors.toList()).get(0).getText();

					currencies.setCurrencyName(currencyName);

					String currencyBuy = webElementAQUA.findElements(By.tagName("td")).stream()
							.filter(webElement -> (webElement.getAttribute("class").equals("cell-buy")
									&& !webElement.getText().contains("%")))
							.collect(Collectors.toList()).get(0).getText().replace(" ", "").replace("Kč", "")
							.replace(",", ".");

					currencies.setBuy(Double.valueOf(currencyBuy));

					String currencySell = webElementAQUA.findElements(By.tagName("td")).stream()
							.filter(webElement -> (webElement.getAttribute("class").equals("cell-sell")
									&& !webElement.getText().contains("%")))
							.collect(Collectors.toList()).get(0).getText().replace(" ", "").replace("Kč", "")
							.replace(",", ".");

					currencies.setSell(Double.valueOf(currencySell));

					currencyTicket.getAllRates().add(currencies);

				});

		String myDateStringEQUA = downloadedData
				.findElement(By.cssSelector("div[class='curr-valid-from__dateSelect datepicker-toggle']")).getText();

		int day = 0;
		int month = 0;
		int year = 0;

		if (myDateStringEQUA != null) {

			String[] myDateArrayStringEQUA = myDateStringEQUA.split("\\.");

			day = Integer.valueOf(myDateArrayStringEQUA[0].trim());
			month = Integer.valueOf(myDateArrayStringEQUA[1].trim());
			year = Integer.valueOf(myDateArrayStringEQUA[2].trim());

		}

		LocalDate myDateEQUA = LocalDate.of(year, month, day);

		String myTimeHoursStringEQUA = downloadedData.findElement(By.id("hours")).getAttribute("value");

		String myTimeMinutesStringEQUA = downloadedData.findElement(By.id("minutes")).getAttribute("value");

		int hours = 0;
		int minutes = 0;
		int seconds = 0;

		if (myTimeHoursStringEQUA != null && myTimeHoursStringEQUA != null) {
			hours = Integer.valueOf(myTimeHoursStringEQUA.trim());
			minutes = Integer.valueOf(myTimeMinutesStringEQUA.trim());
		}

		LocalTime myTimeEQUA = LocalTime.of(hours, minutes, seconds);

		LocalDateTime myDateTimeEQUA = LocalDateTime.of(myDateEQUA, myTimeEQUA);

		currencyTicket.setLastRefresh(myDateTimeEQUA);

		currencyTicket.setBank(EnumBanks.EQUA);
		currencyTicket.setNameOfBank("Equa bank a.s.");

		return currencyTicket;
	}
}
